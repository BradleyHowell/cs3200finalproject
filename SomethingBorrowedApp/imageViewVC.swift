//
//  imageViewVC.swift
//  SomethingBorrowedApp
//
//  Created by School on 4/19/16.
//  Copyright © 2016 CS3200. All rights reserved.
//

import UIKit

class imageViewVC : UIViewController{
    var myImage: UIImage!
    
    @IBOutlet weak var viewImage: UIImageView!
    
    override func viewDidLoad() {
        viewImage.image = myImage
    }
}