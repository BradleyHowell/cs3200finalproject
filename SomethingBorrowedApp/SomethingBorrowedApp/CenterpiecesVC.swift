//
//  CenterpiecesVC.swift
//  SomethingBorrowedApp
//
//  Created by School on 4/14/16.
//  Copyright © 2016 CS3200. All rights reserved.
//

import UIKit

class CenterpiecesVC: UIViewController{
    var cpImages: [UIImage]=[]
    var selectImage = 0
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "viewImage"{
            let imViewVC = segue.destinationViewController as! imageViewVC
            imViewVC.myImage = cpImages[selectImage]
        }
    }
}

extension CenterpiecesVC: UICollectionViewDataSource{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cpImages.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CVCell
        cell.myimage = cpImages[indexPath.row]
        return cell
    }
}

extension CenterpiecesVC: UICollectionViewDelegate{
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        selectImage = indexPath.row
        performSegueWithIdentifier("viewImage", sender: self)
    }
}

