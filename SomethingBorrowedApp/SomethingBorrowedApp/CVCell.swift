//
//  CVCell.swift
//  SomethingBorrowedApp
//
//  Created by School on 4/14/16.
//  Copyright © 2016 CS3200. All rights reserved.
//

import UIKit
class CVCell: UICollectionViewCell{
    
    var myimage = UIImage() {
        didSet {
            cvImage.image = myimage
        }
    }
    @IBOutlet weak var cvImage: UIImageView!
}
