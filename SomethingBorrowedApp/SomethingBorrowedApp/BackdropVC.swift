//
//  BackdropVC.swift
//  SomethingBorrowedApp
//
//  Created by School on 4/8/16.
//  Copyright © 2016 CS3200. All rights reserved.
//

import UIKit
class BackdropVC: UIViewController{
    
    @IBOutlet weak var BDTitle: UILabel!
    @IBOutlet weak var BDPrice: UILabel!
    
    var selectImage = 0
    var bdImages: [UIImage] = []
    var myPrice = "0"
    var myTitle = "na"
    override func viewDidLoad() {
        BDTitle.text = myTitle
        BDPrice.text = myPrice
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "bdviewImage"{
            let imViewVC = segue.destinationViewController as! imageViewVC
            imViewVC.myImage = bdImages[selectImage]
        }
    }
}

extension BackdropVC: UICollectionViewDataSource{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return bdImages.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! bdCVCell
        cell.myimage = bdImages[indexPath.row]
        return cell
    }
}

extension BackdropVC: UICollectionViewDelegate{
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        selectImage = indexPath.row
        performSegueWithIdentifier("bdviewImage", sender: self)    }
}