//
//  bdCVCell.swift
//  SomethingBorrowedApp
//
//  Created by School on 4/21/16.
//  Copyright © 2016 CS3200. All rights reserved.
//

import UIKit
class bdCVCell: UICollectionViewCell{
    
    var myimage = UIImage() {
        didSet {
            bdcvImage.image = myimage
        }
    }
    @IBOutlet weak var bdcvImage: UIImageView!
}