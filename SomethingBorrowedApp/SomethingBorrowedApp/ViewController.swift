//
//  ViewController.swift
//  SomethingBorrowedApp
//
//  Created by School on 4/5/16.
//  Copyright © 2016 CS3200. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var cvImages: [UIImage]=[]
    var bdPrice = "$0"
    var bdTitle = "NA"
    var cntrpcImages: [UIImage] = []
    
    var ouImages: [UIImage] = [UIImage(named:"old europe with fountains.JPG")!,
        UIImage(named:"Old Europe pink & red (2).JPG")!,
        UIImage(named:"new fountain 2.JPG")!,
        UIImage(named:"IMG_6735.jpg")!,
        UIImage(named:"DSCF1199.JPG")!
        ]
    var cbImages: [UIImage] = [UIImage(named:"007.JPG")!,
                               UIImage(named:"P1040306.JPG")!,
                               UIImage(named:"019.JPG")!,
                               UIImage(named:"03MAR_ReceptionOrDinner.JPG")!,
                               UIImage(named:"CambridgeGuestBook.jpg")!,
                               ]
    var sgImages: [UIImage] = [UIImage(named:"100_1684.JPG")!,
                               UIImage(named:"100_1687.JPG")!,
                               UIImage(named:"CIMG1232.JPG")!,
                               UIImage(named:"CIMG3468.JPG")!,
                               ]
    var echImages: [UIImage] = [UIImage(named:"IMG_6896.jpg")!,
                                UIImage(named:"IMG_6848.jpg")!,
                                UIImage(named:"IMG_6921.jpg")!,
                                UIImage(named:"IMG_6851.jpg")!,
                                ]

    @IBAction func ouButton(sender: AnyObject) {
        bdPrice = "$850"
        bdTitle = "Old Europe"
        cvImages = ouImages
        performSegueWithIdentifier("backdropSegue", sender: self)
    }
    @IBAction func cbButton(sender: AnyObject) {
        bdPrice = "$850"
        bdTitle = "Cambridge"
        cvImages = cbImages
        performSegueWithIdentifier("backdropSegue", sender: self)
    }
    @IBAction func sgButton(sender: AnyObject) {
        bdPrice = "$750"
        bdTitle = "Secret Garden"
        cvImages = sgImages
        performSegueWithIdentifier("backdropSegue", sender: self)
    }
    @IBAction func echButton(sender: AnyObject) {
        bdPrice = "$900"
        bdTitle = "Enchanted"
        cvImages = echImages
        performSegueWithIdentifier("backdropSegue", sender: self)
    }
    
    @IBAction func CntPcButton(sender: AnyObject) {        performSegueWithIdentifier("centerpiecesSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //adding Centerpiece images
        cntrpcImages.append(UIImage(named: "100_0132.JPG")!)
        cntrpcImages.append(UIImage(named: "100_0377.JPG")!)
        cntrpcImages.append(UIImage(named: "100_0386.JPG")!)
        cntrpcImages.append(UIImage(named: "100_0449.JPG")!)
        cntrpcImages.append(UIImage(named: "100_0454.JPG")!)
        cntrpcImages.append(UIImage(named: "August 2010 197.JPG")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "centerpiecesSegue"{
            let cpVC = segue.destinationViewController as!CenterpiecesVC
            cpVC.cpImages = cntrpcImages
        }
        if segue.identifier == "backdropSegue"{
           let bdVC = segue.destinationViewController as! BackdropVC
            bdVC.bdImages = cvImages
            bdVC.myPrice = bdPrice
            bdVC.myTitle = bdTitle
        }
    }

}

