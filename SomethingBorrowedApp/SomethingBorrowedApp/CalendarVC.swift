//
//  CalendarVC.swift
//  SomethingBorrowedApp
//
//  Created by School on 4/24/16.
//  Copyright © 2016 CS3200. All rights reserved.
//

import UIKit
import WebKit

class CalendarVC: UIViewController{
    
    @IBOutlet weak var containerView: UIView!

    
    var webView = WKWebView()
    
    override func viewDidLoad() {
        webView.navigationDelegate = self
        
    let url = NSURL(string: "https://calendar.google.com/calendar/render?pli=1#main_7%7Cmonth")!
    let request = NSURLRequest(URL: url)
        webView.loadRequest(request)
        
        containerView.addSubview(webView)
    }
    override func viewWillLayoutSubviews() {
        webView.frame = containerView.bounds
    }
}

extension CalendarVC: WKNavigationDelegate{
//    func webView(webView: WKWebView, decidePolicyForNavigationAction navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
//            decisionHandler(.Cancel)
//    }
    }
